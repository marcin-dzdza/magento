<?php
namespace Marcin\BoolAttribute\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Marcin\BoolAttribute\Model\ResourceModel\Post\Collection as PostCollection;
use \Marcin\BoolAttribute\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use \Marcin\BoolAttribute\Model\Post;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Catalog\Api\CategoryRepositoryInterface;
use \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

class Posts extends Template
{
    /**
     * CollectionFactory
     * @var null|CollectionFactory
     */
    protected $_postCollectionFactory = null;

    protected $productRepository;

    protected $categoryCollectionFactory;

    protected $categoryRepository;

    /**
     * Constructor
     *
     * @param Context $context
     * @param PostCollectionFactory $postCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        PostCollectionFactory $postCollectionFactory,
        ProductRepositoryInterface $productRepository,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) {
        $this->_postCollectionFactory = $postCollectionFactory;
        $this->productRepository = $productRepository;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->categoryRepository = $categoryRepository;
        parent::__construct($context, $data);
    }

    /**
     * @return Post[]
     */
    public function getPosts()
    {
        /** @var PostCollection $postCollection */
        $postCollection = $this->_postCollectionFactory->create();
        $postCollection->addFieldToSelect('*')->load();
        return $postCollection->getItems();
    }

    /**
     * For a given post, returns its url
     * @param Post $post
     * @return string
     */
    public function getPostUrl(
        Post $post
    ) {
        return '/blog/post/view/id/' . $post->getId();
    }

    public function getProductBySku($sku)
    {
        return $this->productRepository->get($sku);
    }

    public function getInCmsByCategoryId($id)
    {
        $category = $this->categoryRepository->get($id);
        $value = $category->getCustomAttribute('in_cms')->getValue();
        return $value;





//        $collection = $this->categoryCollectionFactory->create()->addAttributeToSelect('in_cms');
//        $categories = $collection->addAttributeToFilter('in_cms', $inCms);
//        return $categories;
    }

    public function getCategoriesByInCms($inCms)
    {
        $collection = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('in_cms', $inCms);

        return $collection;
    }

}