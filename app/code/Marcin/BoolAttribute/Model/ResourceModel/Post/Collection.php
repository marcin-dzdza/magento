<?php
namespace Marcin\BoolAttribute\Model\ResourceModel\Post;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Marcin\BoolAttribute\Model\Post', 'Marcin\BoolAttribute\Model\ResourceModel\Post');
    }
}